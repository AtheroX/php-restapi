<?php

class BaseController{

    /**
     * Cuando llamas a cualquier zona no existente entra aquí
     * En PHP lo llaman zona mágica, drogas básicamente
     */
    public function __call($name, $args){
        $this->sendOutput('',array('HTTP/1.1 404 NOT FOUND'));
    }

    protected function getURLSegments(){
        $url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $url = explode('/', $url);
        return $url;
    }

    protected function getQueryParams(){
        parse_str($_SERVER['QUERY_STRING'], $query);
        return $query;
    }

    protected function sendOutput($data, $httpHeaders = array()){
        header_remove('Set-Cookie');
        if(is_array($httpHeaders) && count($httpHeaders)){
            foreach($httpHeaders as $httpHeader){
                header($httpHeader);
            }
        }
        echo $data;
        exit;
    }

}
?>