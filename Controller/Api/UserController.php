<?php
    class UserController extends BaseController{

        /**
         * "/user/getUser"
         */
        public function getUserAction(){
            $errorHead = "";
            $errorDesc = "";
            $responseData = "";
            $reqMethod = $_SERVER['REQUEST_METHOD'];
            $queryParams = $this->getQueryParams();
            
            if(strtoupper($reqMethod)=="GET"){
                try {
                    $userModel = new UserModel();

                    $user = "";
                    if(isset($queryParams['username']) && $queryParams['username']){
                        $user = $queryParams['username'];
                    }

                    $resUser = $userModel->getUser($user);
                    if(empty($resUser)){
                        $errorDesc = "Player not found";
                        $errorHead = "HTTP/1.1 200 OK";
                    }else{
                        $responseData = json_encode($resUser);
                    }

                } catch (Exception $e) {
                    $errorDesc = $e->getMessage();
                    $errorHead = "HTTP/1.1 500 Internal Server Error";
                }
            }else{
                $errorDesc = "Method not supported";
                $errorHead = "HTTP/1.1 422 Unprocessable Entity";
            }

            if(!$errorDesc){
                $this->sendOutput($responseData, array('Content-Type: application/json', 'HTTP/1.1 200 OK'));
            }else{
                $this->sendOutput(
                    json_encode(array('error' => $errorDesc)),
                    array('Content-Type: application/json', $errorHead));
            }
        }

        /**
         * "/user/getUsers"
         */
        public function getUsersAction(){
            $errorDesc = "";
            $errorDesc = "";
            $responseData = "";
            $reqMethod = $_SERVER['REQUEST_METHOD'];
            $queryParams = $this->getQueryParams();
            if(strtoupper($reqMethod)=="GET"){
                try {
                    $userModel = new UserModel();

                    $resUser = $userModel->getUsers();
                    if(empty($resUser)){
                        $errorDesc = "No players on DDBB";
                        $errorHead = "HTTP/1.1 200 OK";
                    }else{
                        $responseData = json_encode($resUser);
                    }

                } catch (Exception $e) {
                    $errorDesc = $e->getMessage();
                    $errorHead = "HTTP/1.1 500 Internal Server Error";
                }
            }else{
                $errorDesc = "Method not supported";
                $errorHead = "HTTP/1.1 422 Unprocessable Entity";
            }

            if(!$errorDesc){
                $this->sendOutput($responseData, array('Content-Type: application/json', 'HTTP/1.1 200 OK'));
            }else{
                $this->sendOutput(
                    json_encode(array('error' => $errorDesc)),
                    array('Content-Type: application/json', $errorHead));
            }
        }
    }
?>