<?php
require_once PROJECT_ROOT_PATH . "/Model/Database.php";

class UserModel extends Database {

    public function getUser($username){
        return $this->select("SELECT * FROM Users WHERE username like ? LIMIT 1", ["s", $username]);
    }

    public function getUsers(){
        return $this->select("SELECT * FROM Users");
    }
}
?>