<?php
class Database{

    protected $connection = null;

    public function __construct() {
        try {
            $this->connection = new mysqli(DB_HOST,DB_USER,DB_PASS,DB_DDBB);
            
        } catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    public function select($query = "", $params = []){

        try {
            $stmt = $this->executeQuery($query, $params);
            $res = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();
            return $res;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }

    private function executeQuery($query = "", $params = []){
        try {
            $stmt = $this->connection->prepare($query);
            if($stmt === false){
                throw new Exception("No se ha podido ejecutar ".$query);
            }
            if($params){
                $stmt->bind_param($params[0],$params[1]);
            }
            $stmt->execute();
            
            return $stmt;
        }catch(Exception $e){
            throw new Exception($e->getMessage());
        }
    }
}
?>