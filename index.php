<?php
require __DIR__ . "/inc/bootstrap.php";

$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$url = explode('/', $url);
// echo implode(" ",$url)."<br>";

// echo 'General '.$url[3];
if (isset($url[3]) && $url[3] == "user") {
    require PROJECT_ROOT_PATH . "/Controller/Api/UserController.php";

    $feedController = new UserController();
    
    if (isset($url[4])){

        $methodName = $url[4].'Action';
        echo $methodName;
        $feedController->{$methodName}();
        
    }else {
        header('HTTP/1.1 404 Not Found');
        exit();
    }
}else {
    header('HTTP/1.1 404 Not Found');
    exit();
}
?>
